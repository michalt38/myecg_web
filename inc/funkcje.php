<?php
	require_once 'conf/zmienne.php';
	require_once 'inc/baza.php';
	//Sprawdzenie czy zalogowano
	function check_login_password($login, $pwd) {
		
		global $baza;
		
		//Sprawdzenie czy się udało
		if($baza->connect_errno > 0){
			die('Nie można połączyć się z bazą [' . $db->connect_error . ']');
		}
		
		$query = "SELECT * FROM `users` WHERE email LIKE '" . $login . "'";
		$wynik = $baza->query($query);
		while($row = $wynik->fetch_assoc())
		{
			if($row["password"] == md5($pwd))
			{
				return $row["id"];
			}
		}
		return '';
	}
	
	function conv($u, $v) {
	    for($i = 0; $i < count($u) + count($v); $i++) {
	        $out[$i] = 0;
	        for($j = 0; $j < count($u); $j++) {
	            if ($i-$j >= 0 && $i-$j < count($v)) {
	                $out[$i] = $out[$i] + $u[$j]*$v[$i-$j];
	            }
	        }
	    }
	    return $out;
	}
	
	function print_array($array) {
	    for($i = 0; $i < count($array); $i++) {
	        echo $array[$i]."<br>";
	    }
	}
	
	function filter($signal, $fc_high, $fc_low, $fs) {
	    $M = 100;
	    $N = 2*$M+1;
	    $fcn_low = 2*$fc_low/$fs;
	    $fcn_high = 2*$fc_high/$fs;
	    //echo "fcn_low ".$fcn_low." fcn_high ".$fcn_high;
	    
	    $iter = 0;
	    for($i = -$M; $i < 0; $i++) {
	        $t1[$iter++] = $i;
	    }
	    $iter = 0;
	    for($i = 1; $i < $M+1; $i++) {
	        $t2[$iter++] = $i;
	    }
	  
	    $iter = 0;
	    for($i = 0; $i < count($t1) + count($t2) + 1;$i++) {
	        if($i < count($t1)) {
	            $h_lp[$i] = sin(2*pi()*$fcn_low*$t1[$iter])/(pi()*$t1[$iter]);
	            $iter++;
	        }else if($i == count($t1)) {
	            $h_lp[$i] = 2*$fcn_low;
	            $iter = 0;
	        }else if($i > count($t1)) {
	            $h_lp[$i] = sin(2*pi()*$fcn_low*$t2[$iter])/(pi()*$t2[$iter]);
	            $iter++;
	        }
	    }
	    
	    $iter = 0;
	    for($i = 0; $i < count($t1) + count($t2) + 1;$i++) {
	        if($i < count($t1)) {
	            $h_hp[$i] = -sin(2*pi()*$fcn_high*$t1[$iter])/(pi()*$t1[$iter]);
	            $iter++;
	        }else if($i == count($t1)) {
	            $h_hp[$i] = 1-2*$fcn_high;
	            $iter = 0;
	        }else if($i > count($t1)) {
	            $h_hp[$i] = -sin(2*pi()*$fcn_high*$t2[$iter])/(pi()*$t2[$iter]);
	            $iter++;
	        }
	    }
	    
	    for($i = 0; $i < $N; $i++) {
	        $w[$i] = 0.54 - 0.46*cos(2*pi()*$i/$N);
	    }

	    for($i = 0; $i < $N; $i++) {
	        $hw_lp[$i] = $w[$i]*$h_lp[$i];
	        $hw_hp[$i] = $w[$i]*$h_hp[$i];
	    }

	    //echo "count signal ".count($signal)."<br>";
	    //print_array($signal);
	    //echo "<br>";
	    //echo "count hw_lp ".count($hw_lp)."<br>";
	    //print_array($hw_lp);
	    
	    $signal_lp = conv($hw_lp, $signal);
	    //echo "count signal_lp ".count($signal_lp)."<br>";
	    //print_array($signal_lp);
	    $signal_lphp = conv($hw_hp, $signal_lp);
	    
	    $n = count($h_hp) + count($h_lp) - 2;
	    
	    for($i = 0; $i < count($signal); $i++) {
	        $signal_shift[$i] = $signal_lphp[$i + $n/2];
	    }
	    
	    return $signal_shift;
	}
	
	function qrs_detection($signal, $times) {
	    $y = array(-1/8, -1/4, 1/4, 1/8);
	    
	    $sig_roz_1 = conv($y, $signal);
	    //print_array($sig_roz);
	    for($i = 0; $i < count($signal); $i++) {
	        $sig_roz[$i] = $sig_roz_1[$i + 2];
	    }
	    $sig_roz_1 = array();
	    
	    for($i = 0; $i < count($sig_roz); $i++) {
	        $sig_pot[$i] = $sig_roz[$i]*$sig_roz[$i];
	    }
	    
	    //print_array($sig_pot);
	    
	    $N = 31;
	    
	    for($i = 0; $i < $N; $i++) {
	        $y2[$i] = 0.54 - 0.46*cos(2*pi()*$i/$N);
	        $y2[$i] = $y2[$i]/$N;
	    }
	    $sig_calk = conv($y2, $sig_pot);
	    $sig_pot = array();
	    $n = count($y2) - 1;
	    for($i = 0; $i < count($signal); $i++) {
	        $sig_calk_shift[$i] = $sig_calk[$i + $n/2];
	    }
	    $sig_calk = array();
	    //print_array($sig_calk_shift);
	    
	    //progowanie sygna�u sca�kowanego
	    $thr1 = 0;
	    $spki = 0;
	    $npki = 0;
	    $RRintervals = array();
	    $interval_counter = 0;
	    $prev_i = 0;
	    
	    for($i = 2; $i < count($sig_calk_shift); $i++) {
	        if($sig_calk_shift[$i-2] < $sig_calk_shift[$i-1] && $sig_calk_shift[$i-1] > $sig_calk_shift[$i]) {
	            
	            if(count($RRintervals) > 0 && array_sum(array_slice($times, $prev_i+1, $i-$prev_i)) > 1.66*(array_sum(array_slice($RRintervals, 0, $interval_counter-2)))) {
	                $peaks = array();
	                $peaks_iter = 0;
	                for($j = $prev_i+2; $j < $i-1; $j++) {
	                    if($sig_calk_shift[$j-2] < $sig_calk_shift[$j-1] && $sig_calk_shift[$j-1] > $sig_calk_shift[$j] && $sig_calk_shift[$j-1] > 0.5*$thr1) {
	                        $peaks[$peaks_iter++] = $sig_calk_shift[$j-1];
	                    }
	                }
	                if(count($peaks) != 0) {
	                    $idx = 0;
	                    $max_val = $peaks[0];
	                    for($i = 1; $it < count($peaks); $it++) {
	                        if($peaks[$it] > $peaks[$it-1]) {
	                            $max_val = $peaks[$it];
	                            $idx = $i;
	                        }
	                    }
	                    $spki = 0.25*$max_val + 0.75*$spki;
	                    $thr1 = $npki + 0.25*($spki - $npki);
	                    
	                    $RRintervals[$interval_counter] = array_sum(array_slice($times, $prev_i+1, $idx-$prev_i-1));
	                    //$RRintervals[$interval_counter-1] = array_sum(array_slice($times, $idx+1, $j-$idx));
	                    $interval_counter++;
	                }
	            }
	            
	            if($sig_calk_shift[$i-1] > $thr1) {
	                if($spki != 0) {
	                    //print_array(array_slice($times, $prev_i+1, $prev_i-i));
	                    $RRintervals[$interval_counter++] = array_sum(array_slice($times, $prev_i+1, $i-$prev_i));
	                }
	                
	                $spki = 0.125*$sig_calk_shift[$i-1] + 0.875*$spki;
	                $thr1 = $npki + 0.25*($spki-$npki);
	                $prev_i = $i-1;
	                
	                
	            } else {
	                $npki = 0.125*$sig_calk_shift[$i-1] + 0.875*$npki;
	                $thr1 = $npki + 0.25*($spki-$npki);
	            }
	        }
	    }
	    
	    //progowanie sygna�u przefiltrowanego
	    $thrf1 = 0;
	    $spkf = 0;
	    $npkf = 0;
	    $RRintervals = array();
	    $interval_counter = 0;
	    $prev_i = 0;
	    
	    for($i = 2; $i < count($signal); $i++) {
	        if($signal[$i-2] < $signal[$i-1] && $signal[$i-1] > $signal[$i]) {
	            
	            if(count($RRintervals) > 0 && array_sum(array_slice($times, $prev_i+1, $i-$prev_i)) > 1.66*(array_sum(array_slice($RRintervals, 0, $interval_counter-2)))) {
	                $peaks = array();
	                $peaks_iter = 0;
	                for($j = $prev_i+2; $j < $i-1; $j++) {
	                    if($signal[$j-2] < $signal[$j-1] && $signal[$j-1] > $signal[$j] && $signal[$j-1] > 0.5*$thr1) {
	                        $peaks[$peaks_iter++] = $signal[$j-1];
	                    }
	                }
	                if(count($peaks) != 0) {
	                    $idx = 0;
	                    $max_val = $peaks[0];
	                    for($i = 1; $it < count($peaks); $it++) {
	                        if($peaks[$it] > $peaks[$it-1]) {
	                            $max_val = $peaks[$it];
	                            $idx = $i;
	                        }
	                    }
	                    $spkf = 0.25*$max_val + 0.75*$spkf;
	                    $thrf1 = $npkf + 0.25*($spkf - $npkf);
	                    
	                    $RRintervals[$interval_counter] = array_sum(array_slice($times, $prev_i+1, $idx-$prev_i-1));
	                    //$RRintervals[$interval_counter-1] = array_sum(array_slice($times, $idx+1, $j-$idx));
	                    $interval_counter++;
	                }
	            }
	            
	            if($signal[$i-1] > $thrf1) {
	                if($spkf != 0) {
	                    $RRintervals[$interval_counter++] = array_sum(array_slice($times, $prev_i+1, $i-$prev_i));
	                }
	                
	                $spkf = 0.125*$signal[$i-1] + 0.875*$spkf;
	                $thrf1 = $npkf + 0.25*($spkf-$npkf);
	                $prev_i = $i-1;
	                
	                
	            } else {
	                $npkf = 0.125*$signal[$i-1] + 0.875*$npkf;
	                $thrf1 = $npkf + 0.25*($spkf-$npkf);
	            }
	        }
	    }
	    
	    //wyznaczanie max sygna�u sca�kowanego
	    $j = 0;
	    $prev_i = 0;
	    $maxima_x = array();
	    $maxima_y = array();
	    for($i = 2; $i < count($sig_calk_shift); $i++) {
	        if($sig_calk_shift[$i-2] < $sig_calk_shift[$i-1] && $sig_calk_shift[$i-1] > $sig_calk_shift[$i] && $sig_calk_shift[$i-1] > $thr1) {
	            
	            if($j == 0){
	                $maxima_x[$j] = $i-1;
	                $maxima_y[$j] = $sig_calk_shift[$i-1];
	                $j++;
	                $prev_i = $i-1;
	                continue;
	            }
	            
	            if(array_sum(array_slice($times, $prev_i+1, $i-$prev_i)) < 200) {
	                if($maxima_y[$j-1] < $sig_calk_shift[$i-1]) {
	                    $maxima_x[$j-1] = $i-1;
	                    $maxima_y[$j-1] = $sig_calk_shift[$i-1];
	                    $prev_i = $i-1;
	                }
	            } else {
	                
	                if(array_sum(array_slice($times, $prev_i+1, $i-$prev_i)) < 360) {
	                    $prev_R = $maxima_x_fil[$j-1];
	                    $k = 1;
	                    $minimum = 0;
	                    while(true) {
	                        if($sig_calk_shift[$prev_R + $k] <= $sig_calk_shift[$prev_R + $k - 1] && $sig_calk_shift[$prev_R + $k] <= $sig_calk_shift[$prev_R + $k + 1]) {
	                            $minimum = $prev_R + $k;
	                            break;
	                        }
	                        $k++;
	                    }
	                    $pik_prev = ($sig_calk_shift[$prev_R] - $sig_calk_shift[$minimum]);
	                    
	                    $curr_R = $i-1;
	                    $k = 1;
	                    $minimum = 0;
	                    while(true) {
	                        if($sig_calk_shift[$curr_R + $k] <= $sig_calk_shift[$curr_R + $k - 1] && $sig_calk_shift[$curr_R + $k] <= $sig_calk_shift[$curr_R + $k + 1]) {
	                            $minimum = $curr_R + $k;
	                            break;
	                        }
	                        $k++;
	                    }
	                    $pik_curr = ($sig_calk_shift[$curr_R] - $sig_calk_shift[$minimum]);
	                    
	                    if($pik_curr < 0.5*$pik_prev)
	                        continue;
	                    else {
	                        $maxima_x[$j-1] = $i-1;
	                        $maxima_y[$j-1] = $sig_calk_shift[$i-1];
	                        $prev_i = $i-1;
	                        continue;
	                    }
	                }
	                
	                $maxima_x[$j] = $i-1;
	                $maxima_y[$j] = $sig_calk_shift[$i-1];
	                $j++;
	                $prev_i = $i-1;
	            }
	        }
	    }
	    //print_array($maxima_x);
	    
	    //wyznaczanie max sygna�u przefiltrowanego
	    $j = 0;
	    $prev_i = 0;
	    $maxima_x_fil = array();
	    $maxima_y_fil = array();
	    for($i = 2; $i < count($signal); $i++) {
	        if($signal[$i-2] < $signal[$i-1] && $signal[$i-1] > $signal[$i] && $signal[$i-1] > $thrf1) {
	            
	            if($j == 0){
	                $maxima_x_fil[$j] = $i-1;
	                $maxima_y_fil[$j] = $signal[$i-1];
	                $j++;
	                $prev_i = $i-1;
	                continue;
	            }
	            
	            if(array_sum(array_slice($times, $prev_i+1, $i-$prev_i)) < 200) {
	                if($maxima_y_fil[$j-1] < $signal[$i-1]) {
	                    $maxima_x_fil[$j-1] = $i-1;
	                    $maxima_y_fil[$j-1] = $signal[$i-1];
	                    $prev_i = $i-1;
	                }
	            } else {
	                
	                if(array_sum(array_slice($times, $prev_i+1, $i-$prev_i)) < 360) {
	                    $prev_R = $maxima_x_fil[$j-1];
	                    $k = 1;
	                    $minimum = 0;
	                    while(true) {
	                        if($signal[$prev_R + $k] <= $signal[$prev_R + $k - 1] && $signal[$prev_R + $k] <= $signal[$prev_R + $k + 1]) {
	                            $minimum = $prev_R + $k;
	                            break;
	                        }
	                        $k++;
	                    }
	                    $pik_prev = ($signal[$prev_R] - $signal[$minimum]);
	                    
	                    $curr_R = $i-1;
	                    $k = 1;
	                    $minimum = 0;
	                    while(true) {
	                        if($signal[$curr_R + $k] <= $signal[$curr_R + $k - 1] && $signal[$curr_R + $k] <= $signal[$curr_R + $k + 1]) {
	                            $minimum = $curr_R + $k;
	                            break;
	                        }
	                        $k++;
	                    }
	                    $pik_curr = ($signal[$curr_R] - $signal[$minimum]);
	                    
	                    if($pik_curr < 0.5*$pik_prev)
	                        continue;
	                    else {
	                        $maxima_x_fil[$j-1] = $i-1;
	                        $maxima_y_fil[$j-1] = $signal[$i-1];
	                        $prev_i = $i-1;
	                        continue;
	                    }
	                }
	                
	                $maxima_x_fil[$j] = $i-1;
	                $maxima_y_fil[$j] = $signal[$i-1];
	                $j++;
	                $prev_i = $i-1;
	            }
	        }
	    }
	    //print_array($maxima_x_fil);
	    
	    //wyznacznie za�amk�w R
	    $x_good = array();
	    for($i = 0; $i < count($maxima_x); $i++) {
	        $x = $maxima_x[$i];
	        
	        if(in_array($x, $maxima_x_fil)) {
	            //echo $x;echo "<br>";
	            $x_good[$i] = $x;
	            continue;
	        }
	        
	        for($odl = 1; $odl < 10; $odl++) {
	            if(in_array($x-$odl, $maxima_x_fil)) {
	                $val = $x-$odl;
	                $x_good[$i] = $val;
	                break;
	            }
	            if(in_array($x+$odl, $maxima_x_fil)) {
	                $val = $x+$odl;
	                $x_good[$i] = $val;
	                break;
	            }
	        }
	    }
	    
	    //print_array($x_good);
	    return $x_good;
	}
	
	function bpm($qrs_count, $time_sec) {
	    $time = $time_sec/60;
	    return round($qrs_count/$time);
	}
	
	function tachogram($peeks, $times) {
	    $tacho = array();
	    for($i = 1; $i < count($peeks); $i++) {
	        $tacho[$i-1] = array_sum(array_slice($times, $peeks[$i-1], $peeks[$i]-$peeks[$i-1]));
	    }
	    return $tacho;
	}
	
	function periodogram_LS($tachogram) {
	    //print_array($tachogram);
	    
	    $time_vec = array();
	    //$time_vec[0] = $tachogram[0];
	    $time_vec[0] = 0;
	    $i = 1;
	    while ($i < count($tachogram)) {
	        $time_vec[$i] = $time_vec[$i-1] + $tachogram[$i];
	        $i++;
	    }
	    
// 	    function tau_f($t, $f0) {
// 	        $part1 = array();
// 	        $part2 = array();
// 	        for($i = 0; $i < count($t); $i++) {
// 	            $part1[$i] = sin(4*pi()*$f0*$t[$i]);
// 	            $part2[$i] = cos(4*pi()*$f0*$t[$i]);
// 	        }
	        
// 	        return (1/(4*pi()*$f0))*atan(array_sum($part1)/array_sum($part2));
// 	    }
	        //$f0=1;
	    //$out=array_map(function($n) use ($f0) {return sin(4*pi()*$f0*$n);}, $time_vec);
	    //print_array($out);
	    $partSin = array();
	    $partCos = array();
	    
	        for($f = 0; $f < 4000; $f++) {
	            $f0 = $f/10000;
	            //print_array(array_map(function($n) {return sin(4*pi()*$f0*$n);}, $time_vec));
	            $tau[$f] = (1/(4*pi()*$f0))*atan(array_sum(array_map(function($n) use ($f0) {return sin(4*pi()*$f0*$n);}, $time_vec))/array_sum(array_map(function($n) use ($f0) {return cos(4*pi()*$f0*$n);}, $time_vec)));
	            //$tau[$f] = tau_f($time_vec, $f0);
	            //$partSin[$f] = pow(array_sum(array_map(function($probe, $n) {return $probe*sin(2*pi()*$f0*($n-$tau[$f]));}, $tachogram, $time_vec)),2)/array_sum(array_map(function($n) {return pow(sin(2*pi()*$f0*($n-$tau[$f])), 2);}, $time_vec));
	            //$partCos[$f] = pow(array_sum(array_map(function($probe, $n) {return $probe*cos(2*pi()*$f0*($n-$tau[$f]));}, $tachogram, $time_vec)),2)/array_sum(array_map(function($n) {return pow(cos(2*pi()*$f0*($n-$tau[$f])), 2);}, $time_vec));
	            
	            $partSin_1 = 0;
	            $partCos_1 = 0;
	            $partSin_2 = 0;
	            $partCos_2 = 0;
	            for($i = 0; $i < count($tachogram); $i++) {
	                $partSin_1 += $tachogram[$i]*sin(2*pi()*$f0*($time_vec[$i]-$tau[$f]));
	                $partCos_1 += $tachogram[$i]*cos(2*pi()*$f0*($time_vec[$i]-$tau[$f]));
	                
	                $partSin_2 += pow(sin(2*pi()*$f0*($time_vec[$i]-$tau[$f])), 2);
	                $partCos_2 += pow(cos(2*pi()*$f0*($time_vec[$i]-$tau[$f])), 2);
	            }
	            $partSin[$f] = pow($partSin_1, 2)/$partSin_2;
	            $partCos[$f] = pow($partCos_1, 2)/$partCos_2;
	            
	            $periodogram[$f] = 0.5*($partSin[$f] + $partCos[$f]);
	        }
	        
	        //print_array($periodogram);
	        return $periodogram;
	    }
	
	function TP($perio) {
	    return array_sum(array_map(function($x) {return abs($x);}, $perio));
	    
	    /* $sum = 0;
	    for($i = 0; $i < count($perio); $i++) {
	        $sum += abs($perio[$i]);
	    }
	    return $sum; */
	}
	
	function HF($perio) {
	    return array_sum(array_map(function($x) {return abs($x);}, array_slice($perio, 1500-1, 4000-1500)));
	}
	
	function LF($perio) {
	    return array_sum(array_map(function($x) {return abs($x);}, array_slice($perio, 400-1, 1500-400)));
	}
	
	function VLF($perio) {
	    return array_sum(array_map(function($x) {return abs($x);}, array_slice($perio, 30-1, 400-30)));
	}
	
	function ULF($perio) {
	    return array_sum(array_map(function($x) {return abs($x);}, array_slice($perio, 0, 30-1)));
	}
	
	function LFHF($perio) {
	    return LF($perio)/HF($perio);
	}
	
	function LFnorm($perio) {
	    return LF($perio)/(TP($perio) - VLF($perio))*100;
	}
	
	function HFnorm($perio) {
	    return HF($perio)/(TP($perio) - VLF($perio))*100;
	}
	
	function RR($tacho) {
	    $RR = 0;
	    for($i = 0; $i < count($tacho); $i++) {
	        $RR += $tacho[$i];
	    }
	    $RR /= count($tacho);
	    return $RR;
	}
	
	function SDNN($tacho) {
	    $RR = RR($tacho);
	    $SDNN = 0;
	    for($i = 0; $i < count($tacho); $i++) {
	        $SDNN += pow($RR - $tacho[$i], 2);
	    }
	    $SDNN /= (count($tacho) - 1);
	    $SDNN = sqrt($SDNN);
	    return $SDNN;
	}
	
	function RMSSD($tacho) {
	    $RMSSD = 0;
	    for($i = 0; $i < count($tacho)-1; $i++) {
	        $RMSSD += pow($tacho[$i+1] - $tacho[$i], 2);
	    }
	    $RMSSD /= (count($tacho) - 1);
	    $RMSSD = sqrt($RMSSD);
	    return $RMSSD;
	}
	
	function NN50($tacho) {
	    $NN50 = 0;
	    for($i = 0; $i < count($tacho)-1; $i++) {
	        if(abs($tacho[$i+1] - $tacho[$i]) > 50)
	            $NN50++;
	    }
	    return $NN50;
	}
	
	function pNN50($tacho) {
	    return ((NN50($tacho)/(count($tacho)-1))*100);
	}
	
	function SD1($x, $y) {
	    $SD1 = 0;
	    $mean_x = array_sum($x)/count($x);
	    $mean_y = array_sum($y)/count($y);
	    for($i = 0; $i < count($x); $i++) {
	        $SD1 += pow(abs(($x[$i]-$mean_x)-($y[$i]-$mean_y))/sqrt(2), 2);
	    }
	    $SD1 /= count($x);
	    $SD1 = sqrt($SD1);
	    return $SD1;
	}
	
	function SD2($x, $y) {
	    $SD2 = 0;
	    $mean_x = array_sum($x)/count($x);
	    $mean_y = array_sum($y)/count($y);
	    for($i = 0; $i < count($x); $i++) {
	        $SD2 += pow(abs(($x[$i]-$mean_x)+($y[$i]-$mean_y))/sqrt(2), 2);
	    }
	    $SD2 /= count($x);
	    $SD2 = sqrt($SD2);
	    return $SD2;
	}
	
	function S($SD1, $SD2) {
	    return pi()*$SD1*$SD2;
	}
	
	function inicjaly($imie, $nazwisko)
	{
		$imie_dobrze = false;
		$nazw_dobrze = false;
		for($i=65;$i<123;$i++){
			if(chr($i) == $imie[0])
				$imie_dobrze = true;
			if(chr($i) == $nazwisko[0])
				$nazw_dobrze = true;
		}
		
		if($imie_dobrze == true and $nazw_dobrze == true)
			return $imie[0].$nazwisko[0];
		if($imie_dobrze == false and $nazw_dobrze == true)
			return $imie[0].$imie[1].$nazwisko[0];
		if($imie_dobrze == true and $nazw_dobrze == false)
			return $imie[0].$nazwisko[0].$nazwisko[1];
		if($imie_dobrze == true and $nazw_dobrze == false)
			return $imie[0].$imie[1].$nazwisko[0].$nazwisko[1];
	}
?>