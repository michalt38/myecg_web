<?php 
    session_start();
    require_once 'conf/zmienne.php';
    require_once 'inc/funkcje.php';
    require_once 'inc/baza.php';
    require_once "inc/nagl.php";
    require_once "inc/menu.php";
    
    $dobry_email = true;
    
    if(isset($_POST["email"])) {
        $query = "select * from users";
        $wynik = $baza->query($query);
        while($row = $wynik->fetch_assoc()) {
            if($row["email"] == $_POST["email"])
                $dobry_email = false;
        }
    }
    
    if(isset($_POST["email"]) && $dobry_email == false) {
?>
		<div class="container">
			<div class="alert alert-danger">
			Given e-mail address already exists.
			</div>
		</div>
<?php
    }
    
    if(isset($_POST["email"]) && $dobry_email == true) {
        $pwd = md5($_POST['password']);
        $is_doctor = 0;
        if($_POST["typeradio"] == 'doctor')
            $is_doctor = 1;
        $query = "insert into `users` (`email`, `password`,`name`,`surname`, `activated`, `is_doctor`) VALUES ('".$_POST['email']."','$pwd','".$_POST['name']."','".$_POST['surname']."', '0', '$is_doctor')";
        //echo $query;
        $baza->query($query);
        /* $query = "select * from `users` where `email`='".$_POST["email"]."'";
        $wynik = $baza->query($query);
        $row = $wynik->fetch_assoc();
        $id_enc = $row['id']; */
        $email_enc = base64_encode($_POST['email']);
        mail($_POST["email"], "Activate myECG account", "Welcome!\n\nPlease activate your account by clicking the following link:\nhttp://student.agh.edu.pl/~milchalt/myECG/activate.php?id=$email_enc\n\nThanks for your registering.");
        //header("Location: index.php?wybrano=0");
    }
    
?>
	<div class="container">
		<h2>Register</h2><br>
		<div class="col-sm-offset-3 col-sm-6">
		<form action="" method="POST" class="form-horizontal">
			<div class="form-group">
      			<label class="col-sm-2 control-label">Name</label>
      			<div class="col-sm-7">
        			<input class="form-control" id="name" name="name" type="text" required="true">
      			</div>
      		</div>
      		<div class="form-group">
      			<label class="col-sm-2 control-label">Surname</label>
      			<div class="col-sm-7">
        			<input class="form-control" id="surname" name="surname" type="text" required="true">
      			</div>
      		</div>
      		<div class="form-group">
      			<label class="col-sm-2 control-label">E-mail</label>
      			<div class="col-sm-7">
        			<input class="form-control" id="email" name="email" type="email" required="true">
      			</div>
      		</div>
      		<div class="form-group">
      			<label class="col-sm-2 control-label">Password</label>
      			<div class="col-sm-7">
        			<input class="form-control" id="password" name="password" type="password" required="true">
      			</div>
      		</div>
      		<div class="form-group">
      			<label class="col-sm-2 control-label">Type</label>
      			<div class="col-sm-7">
        			<div class="radio">
  				<label><input type="radio" name="typeradio" value="patient" checked="checked">Patient</label>
			</div>
			<div class="radio">
  				<label><input type="radio" name="typeradio" value="doctor">Doctor</label>
			</div>
      			</div>
      		</div>
      		<div class="col-sm-offset-4 col-sm-2">
      			<input type="submit" class="btn btn-info" value="Submit">
			</div>
		</form>
		</div>
	</div>
<?php
    require_once "inc/stopka.php";
?>