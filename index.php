<?php 
    session_start();
    require_once 'conf/zmienne.php';
    require_once 'inc/funkcje.php';
    require_once 'inc/baza.php';
    require_once "inc/nagl.php";
    //echo base64_decode("cXdlcnR5");
    
    if($_GET['wyloguj'] == 1)
        session_destroy();
        
    if(isset($_POST['email']) && isset($_POST['password'])){
        $idkonta = check_login_password($_POST['email'], $_POST['password']);
        if($idkonta != '') {
            
            $query = "select * from users where `id`='".$idkonta."'";
            $wynik = $baza->query($query);
            $row = $wynik->fetch_assoc();
            if($row['activated'] != 0) {
                $_SESSION['zalogowany'] = $idkonta;
                $_SESSION['is_doctor'] = $row['is_doctor'];
                $_SESSION['database'] = $idkonta;
            }else
                $byl_blad_logowania = 2;
        }
        else
            $byl_blad_logowania = 1;
    }else if(!isset($_SESSION['zalogowany']))
        session_destroy();
    
    if( !isset($_GET['wybrano']))
        header("Location: index.php?wybrano=0");
    
    require_once "inc/menu.php";
    
    if(!isset($_SESSION['zalogowany'])) {
?>
	<br>
	<h1 class="text-center">myECG</h1>
	<h4 class="text-center">Personal ECG database and analysis platform</h4>
	<br>
<?php 
	if($byl_blad_logowania == 1)
	{
?>
<div class="col-sm-offset-3 col-sm-6">
<div class="alert alert-danger">
  Invalid e-mail or password.
</div>
</div>
<br>
<?php } else if($byl_blad_logowania == 2) {
    ?>
<div class="col-sm-offset-3 col-sm-6">
<div class="alert alert-danger">
  Activate your account.
</div>
</div>
<br>
<?php
    }
?>
	<form action="" method="POST">
	<div class="container">
	<div class="row">
    <div class="col-sm-offset-3 col-sm-6">
    
  		<div class="input-group">
    		<span class="input-group-addon"><i class="fa fa-user"></i></span>
    		<input id="email" type="text" class="form-control" name="email" placeholder="E-mail">
  		</div>
  	<br>
  		<div class="input-group">
    		<span class="input-group-addon"><i class="fa fa-lock"></i></span>
    		<input id="password" type="password" class="form-control" name="password" placeholder="Password">
  		</div>
	<br>
	<div class="row">
		<div class="col-sm-offset-4 col-sm-2"><input type="submit" class="btn btn-info center-block" value="Log In"></div>
		<div class="col-sm-2"><a href="register.php" class="btn btn-link" role="button">Register</a></div>
	</div>
	</div>
	</div>
	</div>
	</form>
	
</div>
<?php
    }
    else
    {
        $query = "select * from users where `id`='".$_SESSION['zalogowany']."'";
        $wynik = $baza->query($query);
        $row = $wynik->fetch_assoc();
        $user_name = $row['name'];
        $user_surname = $row['surname'];
        $user_email = $row['email'];
?>
	<div class="container">
	<h2>Welcome <?php echo $row['name']." ".$row['surname'];?>!</h2>
<?php 
    if($_SESSION['is_doctor'] == 1) {
        
        if($_GET['database'] != '') {
            $_SESSION['database'] = $_GET['database'];
        }
?>
	<br>
	
<?php
    $dobry_email = false;
    $email_enc = "";
	if(isset($_POST["patient_email"])) {
	    $email_enc = $_POST["patient_email"];
        $query = "select * from users";
        $wynik = $baza->query($query);
        while($row = $wynik->fetch_assoc()) {
            if($row["email"] == $_POST["patient_email"]) {
                $dobry_email = true;
                break;
            }
        }
        if($dobry_email) {
            $query = "select * from users where `email`='$email_enc'";
            $wynik = $baza->query($query);
            $row = $wynik->fetch_assoc();
            $id_pat = $row['id'];
            $id_doc = $_SESSION['zalogowany'];
            $query = "select * from access where `doctor`='$id_doc' and `patient`='$id_pat'";
            //echo $query;
            $wynik2 = $baza->query($query);
            if($wynik2->num_rows != 0) $dobry_email = false;
        }
    }
    
    if(isset($_POST["patient_email"]) && $dobry_email == false) {
?>
		<div class="container">
			<div class="alert alert-danger">
			Wrong e-mail address or already added.
			</div>
		</div>
<?php
    }
    
    if(isset($_POST["patient_email"]) && $dobry_email == true) {
        $email_enc = base64_encode($email_enc);
        $user_email_enc = base64_encode($user_email);
        mail($_POST["patient_email"], "Access request", "Hello,\n\nDoctor $user_name $user_surname ($user_email) whant to get an access to your myECG database. To confirm, please click the following link:\nhttp://student.agh.edu.pl/~milchalt/myECG/add_patient.php?idp=$email_enc&idd=$user_email_enc\n\nBest regards");
    }
?>
	<div class="panel panel-default">
  	<div class="panel-heading">
  			<h4 class="panel-title">
				<a data-toggle="collapse" href="#collapse1">Access patient database</a>
			</h4>
	</div>
	<div id="collapse1" class="panel-collapse collapse">
  	<div class="panel-body">
  	<form action="" method="POST">
  <div class="form-group">
    <label for="email">Email address:</label>
    <input type="email" id="patient_email" name="patient_email" class="form-control" id="email">
  </div>
  <button type="submit" class="btn btn-default">Send request</button>
</form>
  	</div>
  	</div>
</div>
<?php
    }
    $my_id = $_SESSION['zalogowany'];
?>
<div class="jumbotron">
<h3>Select database</h3>
<br>
	<input class="form-control" id="myInput" type="text" placeholder="Search..">
  	<br>
	<table class="table">
    		<thead>
     		 	<tr>
        			<th>User name</th>
        			<th>E-mail</th>
        			<th></th>
      			</tr>
    		</thead>
    		<tbody id="myTable">
      			<tr>
        			<td><a href="index.php?wybrano=0&database=<?php echo $my_id?>"><?php echo $user_name." ".$user_surname?></a></td>
        			<td><a href="index.php?wybrano=0&database=<?php echo $my_id?>"><?php echo $user_email;?></a></td>
        			<td><?php 
        			if($_SESSION['database'] == $my_id) {
        			?><span class="label label-primary">Selected</span></td>
        			<?php };?>
      			</tr>
<?php 

        $query = "select * from `access` where `doctor`='$my_id'";
        //echo $query;
        $wynik = $baza->query($query);
        while($row = $wynik->fetch_assoc()) {
            $pat_id = $row['patient'];
            $query2 = "select * from `users` where `id`='$pat_id'";
            //echo $query2;
            $wynik2 = $baza->query($query2);
            $row2 = $wynik2->fetch_assoc();
            echo '<tr><td><a href="index.php?wybrano=0&database='.$pat_id.'">'.$row2['name']." ".$row2['surname'].'</a></td><td><a href="index.php?wybrano=0&database='.$pat_id.'">'.$row2['email']."</a></td>";

            if($_SESSION['database'] == $pat_id) {
                ?>
                <td><span class="label label-primary">Selected</span></td></tr>
                <?php
            } else echo "<td></td></tr>";
            
        }
?>
      			</tbody>
      			</table>
      			</div>
      			
	</div>
	<script>
	$(document).ready(function(){
		  $("#myInput").on("keyup", function() {
		    var value = $(this).val().toLowerCase();
		    $("#myTable tr").filter(function() {
		      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		    });
		  });
		});
	</script>
<?php
    }
    require_once "inc/stopka.php";
?>