<?php 
    session_start();
    
    if(!isset($_SESSION['zalogowany']))
        header("Location: index.php?wybrano=0");
    
    require_once 'conf/zmienne.php';
    require_once 'inc/funkcje.php';
    require_once 'inc/baza.php';
    require_once "inc/nagl.php";
    require_once "inc/menu.php";
    
    if(isset($_POST['delete'])) {
        $id = $_GET['id'];
        $query = "delete from signals_values where `signal`='$id'";
        $baza->query($query);
        $query = "delete from signals_dates where `id`='$id'";
        $baza->query($query);
        header("Location: ecg_database.php?wybrano=1");
    }
    
    if($_SESSION['is_doctor'] == 0)
        $query = "select * from signals_dates where `user`='".$_SESSION['zalogowany']."'";
    else
        $query = "select * from signals_dates where `user`='".$_SESSION['database']."'";
    $wynik = $baza->query($query);
?>
	<div class = "container">
	<br>
	<form action="" method="POST">
	<div class = "panel panel-default">
		<div class = "panel-heading">
			<h4 class="panel-title">
				<a data-toggle="collapse" href="#collapse1">Select an ECG signal</a>
			</h4>
		</div>
		<div id="collapse1" class="panel-collapse collapse">
			<div class = "panel-body">
				<div class="pre-scrollable">
					<table class="table table-hover">
						<tbody>
<?php 
                            while($row = $wynik->fetch_assoc()) {
                               // echo '<tr><td><a href="ecg_database.php?wybrano=1&id='.$row['id'].'>'.$row['date'].'</tr></td>';
?>
							<tr>
								<td>
									<a href="ecg_database.php?wybrano=1&id=<?php echo $row['id'];?>">
										<?php echo $row['date'];?>
									</a>
								</td>
							</tr>
<?php
                            }
?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	</form>
	
<?php 
    if(!isset($_GET['id']))
    {
        echo '</div>';
        require_once "inc/stopka.php";
        return;
    }
    
    $query = "select * from signals_values where `signal`='".$_GET['id']."'";
    $wynik = $baza->query($query);

?>

	<?php 
	$query3 = "select * from signals_values where `signal`='".$_GET['id']."'";
	$wynik3 = $baza->query($query3);
	$iter = 0;
	$time = 0;
	while ($row3 = $wynik3->fetch_assoc()) {
	    $signal[$iter] = $row3["value"];
	    //$time += $row3["time"]/1000000;
	    $times[$iter++] = $row3["time"]/1000;
	}
	$filtered_signal = filter($signal, 5, 15, 200);
	$qrs = qrs_detection($filtered_signal, $times);
	$tacho = tachogram($qrs, $times);
	$perio = periodogram_LS($tacho);
	$x_poincare = array();
	$y_poincare = array();
	$iter = 0;
	//$time = 0;
	while ($iter < count($tacho) - 1) {
	    $x_poincare[$iter] = $tacho[$iter];
	    $y_poincare[$iter] = $tacho[$iter+1];
	    $iter++;
	}
	    ?>
	<div class="tab-content">
	<div id="tabs-1" class="tab-pane fade active in">
      <div id="chartContainerRaw" style="height: 370px; width: 100%;"></div>
    </div>
	<div id="tabs-2" class="tab-pane">
      <div id="chartContainerFiltered" style="height: 370px; width: 100%;"></div>
    </div>
    <div id="tabs-3" class="tab-pane">
      <div id="chartContainerTachogram" style="height: 370px; width: 100%;"></div>
    </div>
    <div id="tabs-4" class="tab-pane">
      <div id="chartContainerPoincare" style="height: 370px; width: 100%;"></div>
    </div>
    </div>
	<br><br>
	<div class="panel panel-default">
  		<div class="panel-body">
  		<ul class="nav nav-pills nav-jt">
  		<li class="active"><a data-toggle="tab" id="bs-tab1" href="#tabs-1">Raw ECG</a></li>
  		<li><a data-toggle="tab" id="bs-tab2" href="#tabs-2">Filtered ECG</a></li>
  		<li><a data-toggle="tab" id="bs-tab3" href="#tabs-3">HRV</a></li>
  		<li><a data-toggle="tab" id="bs-tab4" href="#tabs-4">Poincar&eacute; plot</a></li>
  		<button type="button" class="btn pull-right btn-default" id="deleteButton" data-toggle="modal" data-target="#confirmDeleteModal"><i class="fa fa-trash-o"></i> Delete</button>
  		</ul>
  		</div>
	</div>
	<div class="jumbotron">
	<input class="form-control" id="myInput" type="text" placeholder="Search..">
  	<br>
	<table class="table">
    		<thead>
     		 	<tr>
        			<th>Parameter</th>
        			<th>Value</th>
        			<th>Unit</th>
      			</tr>
    		</thead>
    		<tbody id="myTable">
      			<tr>
        			<td>Heart rate</td>
        			<td><?php echo bpm(count($qrs), array_sum($times)/1000)?></td>
        			<td>bpm</td>
      			</tr>
      			<tr>
        			<td>Mean RR</td>
        			<td><?php echo RR($tacho)?></td>
        			<td>ms</td>
      			</tr>
      			<tr>
        			<td>SDNN</td>
        			<td><?php echo SDNN($tacho)?></td>
        			<td>ms</td>
      			</tr>
      			<tr>
        			<td>RMSSD</td>
        			<td><?php echo RMSSD($tacho)?></td>
        			<td>ms</td>
      			</tr>
      			<tr>
        			<td>NN50</td>
        			<td><?php echo NN50($tacho)?></td>
        			<td></td>
      			</tr>
      			<tr>
        			<td>pNN50</td>
        			<td><?php echo pNN50($tacho)?></td>
        			<td>%</td>
      			</tr>
      			<tr>
        			<td>TP</td>
        			<td><?php echo TP($perio)?></td>
        			<td>ms<sup>2</sup></td>
      			</tr>
      			<tr>
        			<td>HF</td>
        			<td><?php echo HF($perio)?></td>
        			<td>ms<sup>2</sup></td>
      			</tr>
      			<tr>
        			<td>LF</td>
        			<td><?php echo LF($perio)?></td>
        			<td>ms<sup>2</sup></td>
      			</tr>
      			<tr>
        			<td>VLF</td>
        			<td><?php echo VLF($perio)?></td>
        			<td>ms<sup>2</sup></td>
      			</tr>
      			<tr>
        			<td>ULF</td>
        			<td><?php echo ULF($perio)?></td>
        			<td>ms<sup>2</sup></td>
      			</tr>
      			<tr>
        			<td>LF/HF</td>
        			<td><?php echo LFHF($perio)?></td>
        			<td></td>
      			</tr>
      			<tr>
        			<td>LF Norm</td>
        			<td><?php echo LFnorm($perio)?></td>
        			<td>%</td>
      			</tr>
      			<tr>
        			<td>HF Norm</td>
        			<td><?php echo HFnorm($perio)?></td>
        			<td>%</td>
      			</tr>
      			<tr>
        			<td>SD1</td>
        			<td><?php echo SD1($x_poincare, $y_poincare)?></td>
        			<td></td>
      			</tr>
      			<tr>
        			<td>SD2</td>
        			<td><?php echo SD2($x_poincare, $y_poincare)?></td>
        			<td></td>
      			</tr>
      			<tr>
        			<td>S</td>
        			<td><?php echo S(SD1($x_poincare, $y_poincare), SD2($x_poincare, $y_poincare))?></td>
        			<td></td>
      			</tr>
      			<tr>
        			<td>SD2/SD1</td>
        			<td><?php echo SD2($x_poincare, $y_poincare)/SD1($x_poincare, $y_poincare)?></td>
        			<td></td>
      			</tr>
    		</tbody>
  		</table>
	</div>
	<div id="confirmDeleteModal" class="modal fade" role="dialog">
  		<div class="modal-dialog modal-sm">

        <!-- Modal content-->
    	<div class="modal-content">
      	<div class="modal-body">
      		<p class="text-center">Confirm deleting this signal</p>
        	<form action="" method="POST" class="form-horizontal">
        	<div class="text-center">
        	<button type="submit" name="delete" class="btn btn-default">OK</button> <button type="button" class="btn btn-default" data-dismiss="modal" target="#confirmDeleteModal">Cancel</button></p>
        	</div>
        	</form>
      	</div>
    </div>

  </div>
</div>
	</div>
	<script>
	$(document).ready(function(){
		  $("#myInput").on("keyup", function() {
		    var value = $(this).val().toLowerCase();
		    $("#myTable tr").filter(function() {
		      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		    });
		  });
		});
	
	var charts = [];
	window.onload = function () {

		var chart_raw = new CanvasJS.Chart("chartContainerRaw", {
			animationEnabled: true,
			zoomEnabled: true,
			axisX :{
				title: "Time [s]"
			},
			axisY :{
				title: "Value",
				includeZero:false
			},
			data: data
		});
		chart_raw.render();
		charts.push(chart_raw);

	}

	function chartFiltered() {
		var chart_filtered = new CanvasJS.Chart("chartContainerFiltered", {
			animationEnabled: true,
			zoomEnabled: true,
			axisX :{
				title: "Time [s]"
			},
			axisY :{
				title: "Value",
				includeZero:false
			},
			data: data_filtered
		});
		chart_filtered.render();
	}

	function chartTachogram() {
		var chart_tacho = new CanvasJS.Chart("chartContainerTachogram", {
			animationEnabled: true,
			zoomEnabled: true,
			axisX :{
				title: "Time [s]"
			},
			axisY :{
				title: "RR interval [ms]",
				includeZero:false
			},
			data: data_tachogram
		});
		chart_tacho.render();
	}

	function chartPoincare() {
		var chart_poincare = new CanvasJS.Chart("chartContainerPoincare", {
			animationEnabled: true,
			zoomEnabled: true,
			axisX :{
				title: "RR(i) [ms]"
			},
			axisY :{
				title: "RR(i+1) [ms]",
				includeZero:false
			},
			data: data_poincare
		});
		chart_poincare.render();
	}
    
	$('#bs-tab2').on('shown.bs.tab',function(e){
		//var target = $(e.target).attr("href") // activated tab
		//alert(target);
		chartFiltered();
	    $('#bs-tab2').off(); // to remove the binded event after the initial rendering
	    //chart_filtered.chart.width = 0;
	    //chart_filtered.resize();
	    //chart_filtered.render();
	});

	$('#bs-tab3').on('shown.bs.tab',function(e){
		//var target = $(e.target).attr("href") // activated tab
		//alert(target);
		chartTachogram();
	    $('#bs-tab3').off(); // to remove the binded event after the initial rendering
	    //chart_filtered.chart.width = 0;
	    //chart_filtered.resize();
	    //chart_filtered.render();
	});

	$('#bs-tab4').on('shown.bs.tab',function(e){
		//var target = $(e.target).attr("href") // activated tab
		//alert(target);
		chartPoincare();
	    $('#bs-tab4').off(); // to remove the binded event after the initial rendering
	    //chart_filtered.chart.width = 0;
	    //chart_filtered.resize();
	    //chart_filtered.render();
	});
	
	var data_filtered = [];
	var dataSeries_filtered = { type: "line" };
	var dataPoints_filtered = [];
	<?php 
			$x = 0;
			$time = 0;
			while ($x < count($filtered_signal)) {
			    $time += $times[$x]/1000;
			?>
			dataPoints_filtered.push({
					x: <?php echo $time;?>,
					y: <?php echo $filtered_signal[$x++];?>         
				});
			<?php
			}
			?>
			dataSeries_filtered.dataPoints = dataPoints_filtered;
			data_filtered.push(dataSeries_filtered);

			var data_tachogram = [];
			var dataSeries_tachogram = { type: "line" };
			var dataPoints_tachogram = [];
			<?php 
					$x = 0;
					$time = 0;
					while ($x < count($tacho)) {
					    if($x > 0)
					       $time += $tacho[$x];
					?>
					dataPoints_tachogram.push({
							x: <?php echo $time/1000;?>,
							y: <?php echo $tacho[$x++];?>         
						});
					<?php
					}
					?>
					dataSeries_tachogram.dataPoints = dataPoints_tachogram;
					data_tachogram.push(dataSeries_tachogram);

					var data_poincare = [];
					var dataSeries_poincare = { type: "scatter" };
					var dataPoints_poincare = [];
					<?php 
							$x = 0;
							$y = 0;
							$iter = 0;
							//$time = 0;
							while ($iter < count($tacho) - 1) {
							    $x = $tacho[$iter];
							    $y = $tacho[$iter+1];
							    $iter++;
							?>
							dataPoints_poincare.push({
									x: <?php echo $x;?>,
									y: <?php echo $y;?>,
									color: "Blue"      
								});
							<?php
							}
							?>
							dataSeries_poincare.dataPoints = dataPoints_poincare;
							data_poincare.push(dataSeries_poincare);

	var x = 0;
	var data = [];
	var dataSeries = { type: "line" };
	var dataPoints = [];
	<?php 
	$time = 0;
	while ($row = $wynik->fetch_assoc()) {
	    $time += $row['time']/1000000;
	?>
		dataPoints.push({
			x: <?php echo $time;?>,
			y: <?php echo $row['value'];?>         
		});
	<?php
	}
	?>
	dataSeries.dataPoints = dataPoints;
	data.push(dataSeries);
	</script>
<?php 
    require_once "inc/stopka.php";
?>